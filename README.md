Presentation : https://docs.google.com/document/d/1fTludkH3bt7NeG5kSPWAt1-XFUh4ylUVTHiH27rqeQM/edit?usp=sharing

# C\_hild Compiler

[![Run Status](https://api.shippable.com/projects/5a04af779ce1cc0700991dc0/badge?branch=master)](https://app.shippable.com/bitbucket/libinngeorge/mycompiler)

Syntex of C\_hild
----------------------------
All statements are seperated by semicolon (Like C)
## Comments
- comments starts with '#'

```
        e.g. # answer to life, the universe and everything is 42
```
## variable Decleration 
- variable name should not contain any digits or symbols. only Alphabets are allowed for variable naming
- only int and float type declerations are supported , Keywords like int , float, for , printf etc. are reserved

```
         e.g int Foo, bar;
         float FOO, baR;
```
## Expressions
- We can have Expressions which is made of Varibles Const(int or float) and Binary operators like +, -, /, along with (, ).
e.g 
```5 * (3-5)```
- Assigning values(expr) to variable 
e.g ```foo = 5 * (3-5);```
## Loops
### While loop
syntax is 

```
while(<condition>) { <body> }
```
### do While loop
syntex is

```
do {<body>} while(<cond>);
```
### for loop
syntex is 

```
for(<initalization>; <condition>; <increment or decrement operators>) { <body>}
```
## If Else
syntex is 
```
if (<condition>) { <body> } else {<body>}
```
else part is optional means
```
if (<condition>) { <body> } 
```
is allowed
## printf 
syntex is
```
printf(<expr seperated by comma>);
```
e.g 
```printf("bar", foo); # here foo is a variable```

## Conditions 
- operators like && , || for boolean 
- Realtional Operators like < , > , <=, >=, ==, != (like C) are supported
- true and false are also supported

e.g.```
 if ((x==2) && (foo<42)) {x = 3;}
 ```
# Testing C\_hild Compiler
We have included a testfile (test.ch) in the source files. You can run this using the the make command. This will ensure that you have setup your machine correctly.
```
make test   # run the tests on the sample programs here.
make clean  # cleanup the directories of temporary files.

```
# Using The Compiler
You can use our C\_hild compiler using make file to compile and run your code 

```
make SOURCE=<filename without .ch extension>   # runs the <filename>.ch after compiling. compiling creates <filename>.js 

```
If you need to compile without running the program, then you could use 
```
C_hild_compiler <filename>.ch
```
This will create a temp.js file 
