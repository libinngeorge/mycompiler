# Makefile to build all standalone programs.
#
SOURCE=test
COMMON=ast.sml


# Default rules for lex and yacc
#
#
# This rule says that all `.lex.sml` files depend on the corresponding
# `.lex and they can be achieved by running mllex on $<.
%.lex.sml: %.lex
	mllex $<

# Similar to lex

%.grm.sml: %.grm
	mlyacc $<


all: c_hild_compiler target

PHONY: all clean test

clean:
	rm -f *.lex.sml
	rm -f *.grm.sml *.grm.desc *.grm.sig c_hild_compiler
	rm -f *.js

c_hild_compiler: c_hild_compiler.sml c_hild_compiler.mlb c_hild.grm.sml c_hild.lex.sml ${COMMON}
	mlton c_hild_compiler.mlb


test: c_hild_compiler
	${CURDIR}/c_hild_compiler test.ch
	js24 temp.js

target:
	${CURDIR}/c_hild_compiler ${SOURCE}.ch
	mv temp.js ${SOURCE}.js
	js24 ${SOURCE}.js
