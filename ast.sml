(* The abstract syntax tree for expression *)

structure Ast =
struct


    datatype BinOp = Plus | Minus | Mul | Div;

    datatype RelOp = LESSTHAN | GREATERTHAN | GREATERTHANOREQUALTO | LESSTHANOREQUALTO | EQUAL | NOTEQUAL ;

    datatype BoolOP = AND | OR;

    datatype Variable = Var of string

    (* The abstract syntax for expressions *)
    datatype Stmt =   Eqn       of Variable * Expr
                    | IF        of Cond * Stmt list * ElsePart
                    | WHILE     of Cond * Stmt list
                    (*First one is initialisation, then condition, then changes, then block*)
                    | FOR       of Stmt * Cond * Stmt * Stmt list   
                    | DO        of Cond * Stmt list
                    | FloatDecl of Variable list
                    | IntDecl   of Variable list
                    | INCREMENT of Variable
                    | DECREMENT of Variable
                    | PRINT     of Expr list



        and Expr =    Const     of CONST
                    | Op        of Expr * BinOp * Expr


        and Cond =  Bool of Expr * RelOp * Expr 
                    | Con of Cond * BoolOP * Cond 
                    | TRUE 
                    | FALSE


        and ElsePart =    EndIF 
                        | ELSE of Stmt list
      

        and CONST =   Real of real
                    | Int of int
                    | String of string
                    | VAR of Variable ;




    (* Some helper functions *)

    fun plus  a b                  = Op (a, Plus, b)
    fun minus a b                  = Op (a, Minus, b)
    fun mul   a b                  = Op (a, Mul, b)
    fun div_   a b                 = Op (a, Div, b)
    fun lessthan a b               = Bool (a , LESSTHAN, b)
    fun greaterthan a b            = Bool (a ,GREATERTHAN, b)
    fun greaterthanorequalto a b   =  Bool (a , GREATERTHANOREQUALTO, b)
    fun lessthanorequalto a b      =  Bool (a , LESSTHANOREQUALTO, b)
    fun equal a b                  =  Bool (a , EQUAL, b)
    fun notequal a b               =  Bool (a , NOTEQUAL, b)
    fun and_ a b                   = Con(a , AND, b)
    fun or_ a b                    = Con (a , OR, b)
    fun ifpart a b c               = IF (a, b, c)
    fun endif ()                   = EndIF
    fun elsepart a                 = ELSE (a)



    (************************************** 
            Convertng to Java Script 
    ***************************************)

    exception SemanticError of string;

    fun binOpToString Plus  = "+"
      | binOpToString Minus = "-"
      | binOpToString Mul   = "*"
      | binOpToString Div   = "/";


    fun boolOpToString AND  = "&&"
      | boolOpToString OR   = "||";



    fun relOpToString LESSTHAN              = "<"
      | relOpToString GREATERTHAN           = ">"
      | relOpToString GREATERTHANOREQUALTO  = ">="
      | relOpToString LESSTHANOREQUALTO     = "<="
      | relOpToString EQUAL                 = "==="
      | relOpToString NOTEQUAL              = "!=";



    fun Addtoset (Var(x)::xs) decl  = AtomSet.add ( Addtoset xs decl , Atom.atom(x) )
      | Addtoset [] decl            = decl





    fun printvariable (Var(x)) decl = if AtomSet.member (decl, Atom.atom(x))
                                      then 
                                        x
    				                          else 
                                        raise SemanticError ("Undefined variable "^x)




    fun printconst (Real(x)) _    = Real.toString x
      | printconst (Int(x))  _    = Int.toString x
      | printconst (VAR(x)) decl  = printvariable x decl
      | printconst (String(x)) _  = x





    and printExpr (Const(x)) decl         = printconst x decl
      | printExpr (Op(x, oper, y)) decl   = "(" ^ (printExpr x decl) ^ (binOpToString oper) ^ (printExpr y decl) ^ ")"





    and printCond (Bool(x, oper, y)) decl   = "("^(printExpr x decl)^(relOpToString oper)^ (printExpr y decl)^")"
      | printCond (Con(x, oper, y)) decl    = "("^(printCond x decl) ^ (boolOpToString oper) ^ (printCond y decl)^")"
      | printCond (TRUE) decl               = "(true)"
      | printCond (FALSE) decl              = "(false)"




    and printtoString (Eqn(x,y)) decl         = (decl, [printvariable x decl ^"="^ printExpr y decl ^ " ;" ^ "\n" ])
      
      | printtoString (IF(x,y,EndIF)) decl    = (decl, ["if "^(printCond x decl ) ^ " { \n" ^ (String.concatWith "" (printStatement y decl)) ^ "} \n" ])
      
      | printtoString (IF(x,y,ELSE(z))) decl  = (decl, ["if "^(printCond x decl) ^ " {" ^ (String.concatWith "" (printStatement y decl)) ^
    					     "} else { \n" ^ (String.concatWith "" (printStatement z decl))^"} \n" ])
      
      | printtoString (WHILE(x,y)) decl       = (decl, ["while " ^ (printCond x decl) ^ " { \n" ^ (String.concatWith "" (printStatement y decl)) ^ "} \n"])
      
      | printtoString (DO(x,y)) decl          = (decl, [ "do" ^ " { \n" ^ ( String.concatWith "" (printStatement y decl) ) ^ "} while " ^ ( printCond x decl ) ^ "; \n" ] )
      
      | printtoString (FloatDecl(x)) decl     = (Addtoset x decl ,["var " ^ printVarlist x ^ ";\n"])
      
      | printtoString (IntDecl(x)) decl       = (Addtoset x decl ,["var " ^ printVarlist x ^ "\n"])
      
      | printtoString (INCREMENT(x)) decl     = (decl, [printvariable x decl ^ "++" ^ " ;\n"])
      
      | printtoString (DECREMENT(x)) decl     = (decl, [printvariable x decl ^ "--" ^ " ;\n" ])
      
      | printtoString (FOR( Eqn(var,expr), cond, change, block)) decl =
                
                let
                    val tmp = "for ( " ^ (printvariable var decl) ^ " = " ^ (printExpr expr decl) ^ " ; " ^ (printCond cond decl) ^ " ; "
                    val block = "{\n" ^ (String.concatWith "" (printStatement block decl)) ^ "}\n"
                in
                	case change of
                            INCREMENT(x) => (decl, [tmp ^ "++" ^ (printvariable x decl) ^ ") " ^ block ] )
                          | DECREMENT(x) => (decl, [tmp ^ "--" ^ (printvariable x decl) ^ ") " ^ block ] )
                          | _ => raise  SemanticError ("for loop can contain only increment or decrement")
                end
      
      | printtoString (PRINT([])) decl        = (decl ,[""])
     
      | printtoString (PRINT(x)) decl         = (decl, map createPrintStmt (printExpressionlist x decl))
     
      | printtoString _ _                     = raise SemanticError ("Invalid Initalization in For loop")





    and createPrintStmt src   = "print ( " ^ src ^ " );\n"




    and printVarlist (Var(x)::nil)  = x ^ ";"

      | printVarlist (Var(x)::xs)   = x ^ " , " ^ (printVarlist xs)

      | printVarlist []             = " ;"



    and printExpressionlist [] decl         = []

      | printExpressionlist (x::nil) decl   = [printExpr x decl]

      | printExpressionlist (x::xs) decl    = printExpr x decl :: printExpressionlist xs decl



    and printStatement (x::xs) decl =
                      
                      let
                  	val (new_decl, program) = printtoString x decl
                      in
                  	program @ printStatement xs new_decl
                      end

      | printStatement [] decl      = []



    fun compile x = printStatement x AtomSet.empty

end