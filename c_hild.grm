
(* This is the preamble where you can have arbitrary sml code. For us
it is empty *)

%%

    %eop EOF
    %verbose
    %pure
    %name Expr
    %noshift EOF



    (* The TERMINALS or tokens of the language *)

    %term INTCONST of int
        | REALCONST of real
        | VAR of string
        | INT
        | FLOAT
        | STRING of string
        | ASSIGN
        | PLUS
        | DIV
        | MINUS
        | MUL
        | INCREMENT
        | DECREMENT
        | OBRACKET
        | CBRACKET
        | OCURLY
        | CCURLY
        | PRINT
        | TRUE
        | FALSE
        | DO
        | IF
        | ELSE
        | WHILE
        | FOR
        | LESSTHAN
        | GREATERTHAN
        | GREATERTHANOREQUALTO
        | LESSTHANOREQUALTO
        | EQUAL
        | NOTEQUAL
        | AND
        | OR
        | COMMA
        | EOF
        | NEWLINE
        | SEMICOLON




    (* The NONTERMINALS of the language *)

    %nonterm EXP          of Ast.Expr
           | COND         of Ast.Cond
           | STMTS        of Ast.Stmt list
           | STMT         of Ast.Stmt
           | VARLIST      of Ast.Variable list
           | ELSEPART     of Ast.ElsePart
           | PROGRAM      of Ast.Stmt list
           | EXPRESSIONS  of Ast.Expr list




    (*      Operator precedence and associativity. The %left says the operator is
            left associative and the precedence increase as you go down this list.      *)

    %left PLUS MINUS  (* + and - are of same precedence *)
    %left MUL DIV     (* higher than + and - *)
    %left LESSTHAN GREATERTHAN GREATERTHANOREQUALTO EQUAL LESSTHANOREQUALTO NOTEQUAL
    %left AND OR




    (* The type that captures position in the input *)
    %pos int




%%




    PROGRAM : STMTS                ( STMTS )


    STMTS   : STMT STMTS   ( STMT :: STMTS )
            |              ( [] )



    STMT :    VAR ASSIGN EXP SEMICOLON     ( Ast.Eqn ((Ast.Var(VAR)), EXP))
            
            | IF OBRACKET COND CBRACKET OCURLY STMTS CCURLY ELSEPART    (Ast.ifpart COND1 STMTS1 ELSEPART1)
            
            | DO OCURLY STMTS CCURLY WHILE OBRACKET COND CBRACKET SEMICOLON     (Ast.DO ( COND1, STMTS1))
            
            | WHILE OBRACKET COND CBRACKET OCURLY STMTS CCURLY      (Ast.WHILE (COND1, STMTS1))
            
            | FLOAT VARLIST SEMICOLON          ( Ast.FloatDecl(VARLIST1) )
            
            | INT VARLIST SEMICOLON            ( Ast.IntDecl(VARLIST1) )
            
            | INCREMENT VAR SEMICOLON          (Ast.INCREMENT(Ast.Var(VAR)))
            
            | VAR INCREMENT SEMICOLON          (Ast.INCREMENT(Ast.Var(VAR)))
            
            | DECREMENT VAR SEMICOLON          (Ast.DECREMENT(Ast.Var(VAR)))
            
            | VAR DECREMENT SEMICOLON          (Ast.DECREMENT(Ast.Var(VAR)))
            
            | FOR OBRACKET VAR ASSIGN EXP SEMICOLON COND SEMICOLON INCREMENT VAR CBRACKET OCURLY STMTS CCURLY
            			             (Ast.FOR (Ast.Eqn (Ast.Var(VAR1), EXP),COND, Ast.INCREMENT(Ast.Var(VAR2)), STMTS1))

            | FOR OBRACKET VAR ASSIGN EXP SEMICOLON COND SEMICOLON VAR INCREMENT CBRACKET OCURLY STMTS CCURLY
            			             (Ast.FOR (Ast.Eqn (Ast.Var(VAR1), EXP),COND, Ast.INCREMENT(Ast.Var(VAR2)), STMTS1))

            | FOR OBRACKET VAR ASSIGN EXP SEMICOLON COND SEMICOLON DECREMENT VAR CBRACKET OCURLY STMTS CCURLY
            			             (Ast.FOR (Ast.Eqn (Ast.Var(VAR1), EXP),COND, Ast.DECREMENT(Ast.Var(VAR2)), STMTS1))

            | FOR OBRACKET VAR ASSIGN EXP SEMICOLON COND SEMICOLON VAR DECREMENT CBRACKET OCURLY STMTS CCURLY
            			             (Ast.FOR (Ast.Eqn (Ast.Var(VAR1), EXP),COND, Ast.DECREMENT(Ast.Var(VAR2)), STMTS1))

            | PRINT OBRACKET EXPRESSIONS CBRACKET SEMICOLON (Ast.PRINT (EXPRESSIONS))



    VARLIST : VAR COMMA VARLIST   ( (Ast.Var (VAR)) :: VARLIST )
            | VAR                 ( [(Ast.Var (VAR))] )


    EXPRESSIONS :   EXP COMMA EXPRESSIONS   ( EXP :: EXPRESSIONS )
                    | EXP                   ( [EXP] )
                    |                       ( [] )                    (*  when no arguments are passed to printf *)



    EXP     :    INTCONST                ( Ast.Const (Ast.Int(INTCONST)) )
               | REALCONST               ( Ast.Const (Ast.Real(REALCONST)) )
    	       | STRING                  ( Ast.Const ( Ast.String(STRING) ))
    	       | VAR                     ( Ast.Const (Ast.VAR (Ast.Var VAR)) )
    	       | OBRACKET EXP CBRACKET   (EXP)
    	       | EXP PLUS EXP            ( Ast.plus  EXP1 EXP2 )
    	       | EXP MINUS EXP           ( Ast.minus EXP1 EXP2 )
    	       | EXP MUL   EXP           ( Ast.mul   EXP1 EXP2 )
    	       | EXP DIV   EXP           ( Ast.div_  EXP1 EXP2 )



    COND    : OBRACKET COND CBRACKET (COND)
    	| EXP LESSTHAN EXP 		(Ast.lessthan EXP1 EXP2 )
    	| EXP GREATERTHAN EXP  		( Ast.greaterthan EXP1 EXP2)
    	| EXP GREATERTHANOREQUALTO EXP 	( Ast.greaterthanorequalto EXP1 EXP2)
    	| EXP LESSTHANOREQUALTO EXP 	( Ast.lessthanorequalto EXP1 EXP2)
    	| EXP EQUAL EXP                 ( Ast.equal EXP1 EXP2 )
    	| EXP NOTEQUAL EXP   		( Ast.notequal EXP1 EXP2 )
    	| COND AND COND  		( Ast.and_ COND1 COND2 )
    	| COND OR COND  		( Ast.or_ COND1 COND2 )
        | TRUE                  (Ast.TRUE)
        | FALSE                  (Ast.FALSE)


    ELSEPART :  ELSE OCURLY STMTS CCURLY     ( Ast.elsepart STMTS1)
    	       |                             ( Ast.endif () )